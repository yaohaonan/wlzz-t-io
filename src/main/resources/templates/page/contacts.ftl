<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>通讯录</title>
    <script type="text/javascript" src="/wlzzim/contacts/js/jquery-1.8.3.min.js"></script>
    <script src="/wlzzim/layer_mobile/layer.js"></script>
    <link rel="stylesheet" href="/wlzzim/contacts/css/style.css">
    <script src="/wlzzim/contacts/js/hammer.min.js"></script>

    <script type="text/javascript" src="/wlzzim/contacts/js/jquery.charfirst.pinyin.js"></script>
    <script type="text/javascript" src="/wlzzim/contacts/js/sort.js"></script>
    <style>
        .num_name{
            font-size: 14px;
        }

    </style>
    <script>
        function parentDemo(id){
            parent.parentDemo(id);
        }
        function sontoTab() {
            parent.parentToTab(1);
        }
    </script>
</head>
<body id="contactsBody">
<div id="listbox">
    <div id="letter" ></div>
    <div id="ulist" class="sort_box">

    </div>
    <div class="initials">
        <ul>
            <li><img src="/wlzzim/contacts/image/068.png"></li>
        </ul>
    </div>
</div>
<script>
    function getHeadImg(imgUrl){
        if(imgUrl=="/wlzz/images/logo.jpg"){
            return '${domainName}'+imgUrl;
        }else{
            return imgUrl;
        }
    }
    function getAllFriend(){
        var token = androidFun.getToken();
        $.post("http://39.108.97.134:8886/wlzz/wlzzFriends/getFriends",{token:token},function(data) {
            console.log(data);
            var json = data.data;
            console.log(json.length);
     // '[{"img":"one.jpg","name":"老司机"},{"img":"two.jpg","name":"新司机"},{"img":"tx.jpeg","name":"司机"},{"img":"two.jpg","name":"王司机"},{"img":"tx.jpeg","name":"李司机"},{"img":"two.jpg","name":"陈司机"},{"img":"tx.jpeg","name":"赵司机"},{"img":"two.jpg","name":"孙司机"},{"img":"tx.jpeg","name":"胡司机"},{"img":"two.jpg","name":"刘司机"},{"img":"tx.jpeg","name":"马司机"}]';
            $("#ulist").empty();
            for(var i=0;i<json.length;i++){
                $("#ulist").append('<div class="sort_list" onclick="parentDemo(\''+json[i].userid+'\')">'
                        +'<div class="num_logo">'
                        +'<img src="'+getHeadImg(json[i].imgUrl)+'" alt="">'
                        +'</div>'
                        +'<div class="num_name">'+json[i].nickname+'</div>'
                        +'</div>');
            }
            setTimeout("initials()","30");//延时加载
        });
    }

     getAllFriend();
    $(function () {
        var hammertime = new Hammer(document.getElementById("listbox"));
              //为该dom元素指定触屏移动事件
              hammertime.on("swipe", function (ev) {
                       //控制台输出
             //          console.log(ev);
                        sontoTab();
              });
        var hammertime2 = new Hammer(document.getElementById("contactsBody"));
        //为该dom元素指定触屏移动事件
        hammertime2.on("swipe", function (ev) {
            //控制台输出
           // console.log(ev);
            sontoTab();
        });
    });
</script>
</body>
</html>