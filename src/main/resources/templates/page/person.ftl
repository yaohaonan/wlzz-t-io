<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <title>Personal3</title>
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/wlzzim/bui/css/bui.css" />
    <script src="/wlzzim/bui/js/zepto.js"></script>
    <script src="/wlzzim/bui/js/plugins/fastclick.js"></script>
    <script src="/wlzzim/bui/js/bui.js"></script>
    <style type="text/css">
        .personal-info{
            margin-top: 0;
            margin-bottom: .2rem;
        }
        .personal-info .bui-btn {
            padding-top: .2rem;
            padding-bottom: .2rem;
        }
        .personal-info .icon {
            margin-right: 0;
        }
        .contact-list{
            border-top: none;
            margin-bottom: .2rem;
            padding: 0 0.2rem;
            background-color: #ffffff;
        }
        .contact-list .bui-btn{
            padding-left: 0;
            padding-top: .3rem;
            padding-bottom: .3rem;
        }
        .contact-list li:last-child{
            border-bottom: none;
        }
    </style>
    <script>

        var pageview = {};

        bui.ready(function () {

            // 执行初始化
            pageview.init();
        })

        pageview.bind = function () {

        }
        pageview.init = function () {

            this.bind();
        }
    function goChat(){
        var userid = androidFun.getUserId();
        bui.load({
            url: "/wlzzim/index/chat?toUserid=${toUserid}&userId="+userid,
        });
    }
    </script>
</head>
<body>
<div id="page" class="bui-page">
    <#--<header class="bui-bar">-->
        <#--<div class="bui-bar">-->
            <#--<div class="bui-bar-left">-->
                <#--<a class="bui-btn" onclick="goChat()"><i class="icon-back"></i></a>-->
            <#--</div>-->
            <#--<div class="bui-bar-main">联系人信息</div>-->
            <#--<div class="bui-bar-right">-->
            <#--</div>-->
        <#--</div>-->
    <#--</header>-->
    <main>
        <ul class="bui-list personal-info">
            <li class="bui-btn bui-box">
                <div class="thumbnail"><img src="${imgUrl!""}" alt=""></div>
                <div class="span1">
                    <h3 class="item-title">${nickName!""}</h3>
                    <p class="item-text">${schoolName!""}</p>
                </div>
            </li>
        </ul>
        <ul class="bui-list contact-list">
            <li class="bui-btn bui-box clearactive">
                <label class="bui-label">性别</label>
                <div class="span1">
                    <div class="bui-value">${sex!""}</div>
                </div>
            </li>
            <li class="bui-btn bui-box clearactive">
                <label class="bui-label">座右铭</label>
                <div class="span1">
                    <div class="bui-value">${motto!""}</div>
                </div>
            </li>
        </ul>
        <#--<div class="container-xy">-->
            <#--<div class="bui-btn round primary" onclick="self.location=document.referrer;">返回聊天</div>-->
        <#--</div>-->
    </main>


</div>

</body>
</html>
