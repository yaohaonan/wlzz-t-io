<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,initial-scale=1.0,width=device-width"/>
    <meta name="format-detection" content="telephone=no,email=no,date=no,address=no">
    <title>聊天详情</title>
    <script src="/wlzzim/js/jquery-1.11.1.min.js"></script>
    <script src="/wlzzim/layer/layer.js"></script>
    <link rel="stylesheet" href="/wlzzim/bui/css/bui.css" />
    <style>
        .iconfont{
            font-family:"iconfont" !important;
            font-size:16px;font-style:normal;
            -webkit-font-smoothing: antialiased;
            -webkit-text-stroke-width: 0.2px;
            -moz-osx-font-smoothing: grayscale;}
        .page-chat main {
            background: #F4F5F7; }

        .chat-panel {
            padding: 0 .1rem;
            border-top:0;
            background: #F4F5F7; }
        .chat-panel .time {
            margin: .2rem 0;
            padding: 0 .2rem;
            height: .2.5rem;
            border-radius: .15rem;
            background: #d8d8d8;
            color: #fff;
            font-size: .18rem; }
        .chat-panel .chat-icon {
            width: .6rem;
            height: .6rem; }
        .chat-panel .bui-box-align-top {
            margin-bottom: .2rem; }
        .chat-panel .chat-content {
            display: inline-block;
            word-wrap: break-word;
            word-break: normal;
            width: 200px;
            background: #fff;
            -webkit-border-radius: .05rem;
            border-radius: .05rem;
            position: relative;
            padding: .15rem; }
        .chat-panel .chat-target .chat-content {
            margin-right: 1.4rem;
            margin-left: .2rem; }
        .chat-panel .chat-target .chat-content.bui-arrow-left:before {
            border: none; }
        .chat-panel .chat-target .chat-content.bui-arrow-left:before, .chat-panel .chat-target .chat-content.bui-arrow-left:after {
            top: .1rem;
            margin-top: 0; }
        .chat-panel .chat-mine .chat-content {
            margin-left: 1.4rem;
            margin-right: .2rem;
            background: #B3EAFF; }
        .chat-panel .chat-mine .chat-content.bui-arrow-right:before, .chat-panel .chat-mine .chat-content.bui-arrow-right:after {
            top: .1rem;
            margin-top: 0;
            border-left-color: #B3EAFF; }
        .chat-panel .chat-mine .chat-content.bui-arrow-right:before {
            border: none; }

        .chatbar {
            padding: .12rem; }
        .chatbar .bui-input i {
            width: .7rem;
            height: .46rem;
            line-height: .46rem;
            font-size: .46rem; }
        .chatbar .bui-input .bui-btn {
            display: inline-block;
            padding-left:0;
            padding-right:0;
            width: .7rem;
            border:0;
        }
        .chatbar .bui-input input {
            border: 1px solid #dedede;
            margin: 0 .12rem 0 0;
            padding: .2rem;
            -webkit-border-radius: .05rem;
            border-radius: .05rem;
            padding-right: .1rem;
            padding-left: .1rem; }
        footer {
            background: #fff;
        }
        #pictureBtn{
           background-color: #52a4ff;
            color: #fff;
        }
    </style>
    <!-- 依赖库 手机调试的js引用顺序如下 -->
    <script src="/wlzzim/bui/js/zepto.js"></script>
    <script src="/wlzzim/bui/js/plugins/fastclick.js"></script>
    <!-- BUI库 -->
    <script src="/wlzzim/bui/js/bui.js"></script>
    <script src="/wlzzim/bui/js/app/_config.js"></script>
    <script>
        function getHeadImg(imgUrl){
            if(imgUrl=="/wlzz/images/logo.jpg"){
                return '${domainName}'+imgUrl;
            }else{
                return imgUrl;
            }
        }
        function dateFormat(temp){
            var date = new Date(temp);
            Y = date.getFullYear() + '年';
            M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '月';
            D = date.getDate() + '日';
            h = date.getHours() + '时';
            m = date.getMinutes() + '分';
            s = date.getSeconds()+'秒';
            return (Y+M+D+h+m+s);
        }
        function isDisplayDate(temp1,temp2){
            if((temp1-temp2)>5*60*1000){
                return true;
            }
            return false;
        }
        function personInfo(id) {
           // androidFun.newPage("个人信息","http://39.108.97.134:8801/wlzzim/index/person?toUserid="+id);
            androidFun.gotoUserInfoPage(id);
        }
        var pageview = {};
        bui.ready(function(){
            // 业务逻辑需要在这里写
            pageview.init();
            // pullRefresh 初始化
            var uiPullRefresh;
            uiPullRefresh = bui.pullrefresh({
                id: "#pullrefresh",
                onRefresh : getData
            });
            // 刷新以后触发
            function getData () {
                var _self = this;
                // 刷新以后触发
                var page =  document.getElementById("page").value;
                var size =  document.getElementById("size").value;
                // var userid =  document.getElementById("userid").value;  //自己将不再采用userid模式

                 var token = androidFun.getToken();
                 var toUserid =  document.getElementById("toUserid").value;
                $.post("/wlzzim/index/reloadChat",{token:token,toUserid:toUserid,page:page,size:size},function (data) {
                    data = data.replace(/&nbsp;/g," ");   //替换转义字符
                    console.log(data);
                    var obj = eval('(' + data + ')');

                    if(obj.flag==2){   //单聊记录
                        var toUserid = document.getElementById("toUserid").value;
                        var dataList = obj.dataList;
                        var allContent = "";
                        for(var i = 0;i<dataList.length;i++){
                            var html = "";
                            var j = i+1;
                            if(j>=dataList.length){
                                //忽略不做处理
                            }else{
                                if(isDisplayDate(dataList[j].create_time,dataList[i].create_time)){
                                    html+=  '<div class="bui-box-center">'+
                                            '                <div class="time">'+dateFormat(dataList[i].create_time)+'</div>'+
                                            '            </div>';
                                }
                            }
                            if(toUserid!=dataList[i].toUserid){  //左边
                                html += '<div class="bui-box-align-top chat-target">';
                                html += '    <div class="chat-icon personInfo" onclick="personInfo(\'${toUserid}\')"><img src="${toUserImg}" alt=""></div>';
                                html += '    <div class="chat-content bui-arrow-left">' + dataList[i].content + '</div>';
                                html += '    <div class="span1">';
                                html += '    </div>';
                                html += '</div>';
                            }else{  //否则就是右边
                                html += '<div class="bui-box-align-top chat-mine">';
                                html += '    <div class="span1">';
                                html += '    </div>';
                                html += '    <div class="chat-content bui-arrow-right">' + dataList[i].content + '</div>';
                                html += '    <div class="chat-icon personInfo" onclick="personInfo(\'${userId}\')"><img src="${userImg}" alt=""></div>';
                                html += '</div>';
                            }
                            allContent = allContent+html;
                        }

                        document.getElementById("chat-panel-content").innerHTML = allContent+document.getElementById("chat-panel-content").innerHTML;
                    }
                    _self.reverse();
                    var page = document.getElementById("page").value;
                    if(page==0){
                        document.querySelector("#pullrefresh").scrollTop = document.querySelector("#pullrefresh").scrollHeight;
                    }
                    document.getElementById("page").value = page-0+1;
                });
            }
            //生成列表模板
            function templateList (data) {
                var html = "";
               console.log("刷新成功");
            }

        });
        pageview.init = function () {
            this.bind();
        }
        pageview.bind = function () {
            // 发送的内容
            var $chatInput = $("#chatInput"),
                    // 发送按钮
                    $btnSend = $("#btnSend"),
                    // 聊天的容器
                    $chatPanel = $(".chat-panel");

            // 绑定发送按钮
            $btnSend.on("click",function (e) {
                var val = $chatInput.val();
                var tpl = chatTpl(val);
                if( !$(this).hasClass("disabled") ) {
                    $chatPanel.append(tpl);
                    $chatInput.val('');
                    $(this).removeClass("primary").addClass("disabled");
                     document.querySelector("#pullrefresh").scrollTop = document.querySelector("#pullrefresh").scrollHeight;
                }else{
                    return false;
                }
            });

            // 延迟监听输入
            $chatInput.on("input",bui.unit.debounce(function () {
                var val = $chatInput.val();
                if( val ){
                    $btnSend.removeClass("disabled").addClass("primary");

                }else{
                    $btnSend.removeClass("primary").addClass("disabled");

                }
            },100));


        }

        // 聊天模板
        var chatTpl = function (con,type) {
            var html = "";
            var type = type || 0;
            tiows.send(con);// 發送消息
            switch(type) {
                case 0:
                    html += '<div class="bui-box-align-top chat-mine">';
                    html += '    <div class="span1">';
                    html += '    </div>';
                    html += '    <div class="chat-content bui-arrow-right">' + con + '</div>';
                    html += '    <div class="chat-icon personInfo" onclick="personInfo(\'${userId}\')"><img src="${userImg}" alt=""></div>';
                    html += '</div>';
                    break;
                case 1:
                    html += '<div class="bui-box-align-top chat-target">';
                    html += '    <div class="chat-icon personInfo"><img src="${toUserImg}" alt=""></div>';
                    html += '    <div class="chat-content bui-arrow-left">' + con + '</div>';
                    html += '    <div class="span1">';
                    html += '    </div>';
                    html += '</div>';
                    break;
            }
            return html;
        }

    </script>
</head>
<body>
<input hidden id="server_ip" value="${server_ip}">
<input hidden id="server_port" value="${server_port}">
<input hidden id="toUserid" value="${toUserid}">
<input hidden id="page" value="0">
<input hidden id="size" value="10">
<div id="page" class="bui-page page-chat">
    <#--<header class="bui-bar">-->
        <#--<div class="bui-bar-left">-->
            <#--<a class="bui-btn" href="/wlzzim/index/dialogue"><i class="icon-back"></i></a>-->
        <#--</div>-->
        <#--<div class="bui-bar-main">-->
            <#--聊天详情-->
        <#--</div>-->
        <#--<div class="bui-bar-right personInfo">-->
        <#--<a class="bui-btn"><i class="icon-face"></i>-->
        <#--</a>-->
    <#--</div>-->
    <#--</header>-->
    <main>
        <div id="pullrefresh" class="bui-scroll">
            <div class="bui-scroll-head"></div>
            <div class="bui-scroll-main" >
                <div class="bui-panel chat-panel" id="chat-panel-content">

                </div>
            </div>
        </div>

    </main>
    <footer>
        <div class="chatbar">
            <div class="bui-input">
                <textarea id="chatInput"></textarea>
                <span class="bui-btn round" id="pictureBtn" onclick="uploadPic();">图片</span>
                <#--<span onclick="uploadPic();"><i class="icon-picfill" id="pictureIcon"></i></span>-->
                <span id="btnSend" class="bui-btn round disabled">发送</span>

            </div>
        </div>
    </footer>
</div>

</body>
<script>
    function uploadPic() {
        // 1. 初始化 这里如果传url初始化,则url作为公共上传地址,start不再需要传url
        var uiUpload = bui.upload({
            url: "/wlzzim/index/uploadImg"
        });
        // 2. 选择文件
        uiUpload.add({
            onSuccess: function(file){
                 console.log(this)
                uiUpload.toBase64({
                    onSuccess: function (imgurl) {
                         console.log(imgurl);
                        // 上传文件 选择以后直接上传到服务器
                        uiUpload.start({
                            onSuccess:function(data){
                                console.log("图片上传成功：" +data);
                                var content = '<img &nbsp; src=\''+data+'\'&nbsp; width=\'150px\'&nbsp; height=\'150px\' >'
                                tiows.send(content);// 發送消息
                                var html ="";
                                html += '<div class="bui-box-align-top chat-mine">';
                                html += '    <div class="span1">';
                                html += '    </div>';
                                html += '    <div class="chat-content bui-arrow-right"><img src="'+imgurl+'" width="150px" height="150px" >' + '</div>';
                                html += '    <div class="chat-icon personInfo" onclick="personInfo(\'${userId}\')"><img src="${userImg}" alt=""></div>';
                                html += '</div>';
                                document.getElementById("chat-panel-content").innerHTML = document.getElementById("chat-panel-content").innerHTML+html;
                                document.querySelector("#pullrefresh").scrollTop = document.querySelector("#pullrefresh").scrollHeight;
                            },
                            onFail: function(res,status){
                                // 失败 status = "timeout" || "error" || "abort", "parsererror"

                            }
                        });

                    }
                });


            }
        })
    }
</script>
<!-- 组件js，和业务不相关的 -->
<script src="/wlzzim/tio/js/toUserDialogue/tiows.js"></script>
<!-- 和业务相关的js，业务需要修改 -->
<script>
    // 给输入框绑定事件,点击的时候触发一个定时器,把输入框滚动到可视范围内
    $('.bui-input').on('click', function () {
        $("html,body").css("overflow","scroll");
        var target = this;
        // 使用定时器是为了让输入框上滑时更加自然
        setTimeout(function(){
            target.scrollIntoView(true);
        },100);
    });
    var DemoHandler = function () {
        this.onopen = function (event, ws) {
            // ws.send('hello 连上了哦')
            console.log("连接成功");
        }
        /**
         * 收到服务器发来的消息
         * @param {*} event
         * @param {*} ws
         */
        this.onmessage = function (event, ws) {

            var data = event.data;
            data = data.replace(/&nbsp;/g," ");
            console.log(data);
            var obj = eval('(' + data + ')');
            if(obj.flag==1){  //所在好友发来消息内容
                var dataList = obj.data;
                var allContent = "";
                console.log(data);
                var html = "";
                var toUserid = document.getElementById("toUserid").value;
                // var userid = document.getElementById("userid").value;

                if(toUserid==obj.userid){  //左边  (当前对话框有新的消息)
                    html += '<div class="bui-box-align-top chat-target">';
                    html += '    <div class="chat-icon personInfo" onclick="personInfo(\'${toUserid}\')"><img src="${toUserImg}" alt=""></div>';
                    html += '    <div class="chat-content bui-arrow-left" >' + obj.content + '</div>';
                    html += '    <div class="span1">';
                    html += '    </div>';
                    html += '</div>';
                    $.post("/wlzzim/index/userGetMsg",{_id:obj.id},function (data) {
                        console.log("您已接受消息：："+data);
                        var array=new Array(0,300,50,100);
                        androidFun.vibrate(-1,array);
                    })
                }
                document.getElementById("chat-panel-content").innerHTML = document.getElementById("chat-panel-content").innerHTML+html;
                document.querySelector("#pullrefresh").scrollTop = document.querySelector("#pullrefresh").scrollHeight;
            }else if(obj.flag==2){   //单聊记录
                var toUserid = document.getElementById("toUserid").value;
                var dataList = obj.dataList;
                var allContent = "";
                for(var i = 0;i<dataList.length;i++){
                    var html = "";
                    if(toUserid!=dataList[i].toUserid){  //左边
                        html += '<div class="bui-box-align-top chat-target">';
                        html += '    <div class="chat-icon personInfo" onclick="personInfo(\'${toUserid}\')"><img src="${toUserImg}" alt=""></div>';
                        html += '    <div class="chat-content bui-arrow-left">' + dataList[i].content + '</div>';
                        html += '    <div class="span1">';
                        html += '    </div>';
                        html += '</div>';
                    }else{  //否则就是右边
                        html += '<div class="bui-box-align-top chat-mine">';
                        html += '    <div class="span1">';
                        html += '    </div>';
                        html += '    <div class="chat-content bui-arrow-right">' + dataList[i].content + '</div>';
                        html += '    <div class="chat-icon personInfo" onclick="personInfo(\'${userId}\')"><img src="${userImg}" alt=""></div>';
                        html += '</div>';
                    }
                    allContent = allContent+html;
                }
                console.log(allContent);
                document.getElementById("chat-panel-content").innerHTML = allContent;
            }



        }


        this.onclose = function (e, ws) {
            // error(e, ws)
        }

        this.onerror = function (e, ws) {
            // error(e, ws)
        }

        /**
         * 发送心跳，本框架会自动定时调用该方法，请在该方法中发送心跳
         * @param {*} ws
         */
        this.ping = function (ws) {
            // log("发心跳了")
            ws.send('心跳内容')
        }
    }
</script>
<script src="/wlzzim/tio/js/toUserDialogue/demo.js"></script>
</html>
