package com.websocket.util;

import java.io.*;
import java.util.Calendar;

public class FileSaveLocalUtil {
    public static void savePic(InputStream inputStream, String path,String fileName) {
        OutputStream os = null;
        try {
            // 1、保存到临时文件
            // 1K的数据缓冲
            byte[] bs = new byte[1024];
            // 读取到的数据长度
            int len;
            // 输出的文件流保存到本地文件
            File tempFile = new File("/root/wlzz/taskImage"+path);
            if (!tempFile.exists()) {
                tempFile.mkdirs();
            }
            String result = tempFile.getPath() + File.separator + fileName+".jpg";
            os = new FileOutputStream(result);
            // 开始读取
            while ((len = inputStream.read(bs)) != -1) {
                os.write(bs, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 完毕，关闭所有链接
            try {
                os.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
    public static  String getYearAndMonth(){
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String path = "/"+year + "_" + month + "/";
        return path;
    }

}
