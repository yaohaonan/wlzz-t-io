package com.websocket.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author YHN
 * @version 2018-03-27
 */
@Entity
@Data  //lombok表达式注解(省略setter和getter)
public class UserInfo {  //个人基本信息
    @Id
    @GeneratedValue
    private Integer userInfoId;//用户信息id(自增)，用来排序
    private String nickName; //昵称
    private String realName; //真实姓名
    private String rollNumber; //roll账号
    private String imgUrl;  //头像
    private String sex;  //性别
    private Date birth; //生日
    private String motto; //座右铭
    private String schoolName;//学校
    private String departmentName; //院系
    private String majorName; //专业
    private String sno;  //学号
    private String snoPassword;  //学号的密码
    private Integer userId;  //逻辑外键指定用户
}
