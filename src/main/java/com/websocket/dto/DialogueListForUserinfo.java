package com.websocket.dto;

import lombok.Data;

@Data
public class DialogueListForUserinfo{
    private DialogueList dialogueList;
    private String imgUrl; //头像
    private String nickname; //名字
}
