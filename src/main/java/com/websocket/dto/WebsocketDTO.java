package com.websocket.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by yhn on 2018/6/8.
 */
@Data
public class WebsocketDTO<T>{  //websocket统一标准输出格式
    private Integer flag;  //消息标志
    private List<T> data;
}
