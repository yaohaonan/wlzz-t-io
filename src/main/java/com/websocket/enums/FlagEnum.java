package com.websocket.enums;

import lombok.Getter;

/**
 * Created by Acer on 2018/6/8.
 */
@Getter
public enum FlagEnum {
    NO_DELETE(10,"未删除"),
    DELETE(20,"已删除"),
    ;
    private Integer code;

    private String message;

    FlagEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
