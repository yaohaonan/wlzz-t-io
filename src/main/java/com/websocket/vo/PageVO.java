package com.websocket.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by yhn on 2018/6/19.
 */
@Data
public class PageVO<T> {
    private List<T> dataList;  //数据
    private Integer currentPage;  // 当前页数
    private Integer totalPage;  //总页数
    private Integer size; //限制大小
    private int flag;   //消息标志   //2
}
