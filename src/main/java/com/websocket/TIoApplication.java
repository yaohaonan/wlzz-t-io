package com.websocket;

import com.websocket.t_io.websocket.ShowcaseWebsocketStarter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TIoApplication {
	public static void main(String[] args) {
		SpringApplication.run(TIoApplication.class, args);
		new Thread(()->{
			try {
				ShowcaseWebsocketStarter.start();
			}catch (Exception e){
				e.printStackTrace();
				System.err.println("t-io开启失败");
			}
		}).start();

	}
}
