package com.websocket.repository;
import com.websocket.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface UserInfoRepository extends JpaRepository<UserInfo,Integer> {
     UserInfo findByUserId(Integer userId);
}
