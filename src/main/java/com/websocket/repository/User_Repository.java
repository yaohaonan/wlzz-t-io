package com.websocket.repository;
import com.websocket.entity.User_;
import org.springframework.data.jpa.repository.JpaRepository;
public interface User_Repository extends JpaRepository<User_,Integer> {
     User_ findByPhone(String phone);
}
