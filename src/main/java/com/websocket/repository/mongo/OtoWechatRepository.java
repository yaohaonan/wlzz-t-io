package com.websocket.repository.mongo;
import com.websocket.dto.WechatDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;
public interface OtoWechatRepository extends MongoRepository<WechatDTO,String>{
   List<WechatDTO> findByToUseridAndFlag(String userid, int flag); //查询该用户所有未接收的消息
   Page<WechatDTO> findByUseridAndToUseridOrUseridAndToUserid(String userid, String toUserid,String userid2, String toUserid2, Pageable pageable);  //查询单对单的聊天记录
   List<WechatDTO> findByUseridAndToUseridAndFlag(String userid,String toUserid,int flag);   //查询该用户与消息用户的未读消息
   WechatDTO findBy_id(String _id);
}
