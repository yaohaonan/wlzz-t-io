package com.websocket.constant;

/**
 * Created by yhn on 2017/9/5.
 */
public interface CookiesConstant {
    String TOKEN = "token";
    String CODE = "code";
    String LOGIN_CODE = "logincode";
    Integer EXPIRE = 7200;
    Integer CODE_EXPIRE = 300;
}
