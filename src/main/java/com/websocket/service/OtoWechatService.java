package com.websocket.service;

import com.websocket.dto.WechatDTO;
import com.websocket.vo.PageVO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by yhn on 2018/6/15.
 */
public interface OtoWechatService {
    List<WechatDTO> findByToUseridAndFlag(String userid, int flag); //查询未接收的消息
    PageVO<WechatDTO> findByUseridAndToUserid(String userid, String toUserid,String toUserid2,String userid2, Pageable pageable);  //查询单对单的聊天记录
    WechatDTO userGetMsg(String _id);
}
