package com.websocket.service.Impl;

import com.websocket.entity.User_;
import com.websocket.repository.User_Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl {
    @Autowired
    private User_Repository userRepository;
    public User_ findByPhone(String phone){
        return userRepository.findByPhone(phone);
    }
}
